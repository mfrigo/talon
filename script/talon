#!/usr/bin/env python
import pkgutil
import talon.cli.commands
import talon.cli.utils

DESCRIPTION = """\
Tractograms As Linear Operators in Neuroimaging - command line interface
"""

EPILOG = """
Copyright: CoBCoM 2021.
"""


def parse_arguments():
    parser = talon.cli.utils.setup_parser(prog='talon',
                                          description=DESCRIPTION,
                                          epilog=EPILOG)
    parser.add_argument(
        '--version', action='version', version=f'%(prog)s {talon.__version__}')

    subparsers = parser.add_subparsers()
    subparsers.required = True
    subparsers.dest = 'subcommand'

    # Load all the sub commands from the talon.cli.commands package
    # dynamically.
    package = talon.cli.commands
    prefix = package.__name__ + '.'
    for _, name, _ in pkgutil.iter_modules(package.__path__, prefix):
        module = __import__(name, fromlist=['nothing'])
        module.add_parser(subparsers)

    return parser.parse_args()


def main():
    args = parse_arguments()
    talon.cli.utils.parse_verbosity(args)

    parameters = {k: v for k, v in vars(args).items() if k != 'func'}
    args.func(**parameters)


if __name__ == '__main__':
    main()
