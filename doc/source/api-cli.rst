====================
CLI module
====================
These functions are available at the ``talon.cli`` module, which must be
imported separately.

.. code:: python

    # import talon
    import talon.cli

Utils
=====

.. automodule:: talon.cli.utils
    :members:

Commands
========

Filter
------
.. automodule:: talon.cli.commands.filter
    :members: run

Voxelize
--------
.. automodule:: talon.cli.commands.voxelize
    :members: run
