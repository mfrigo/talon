.. _command-line-interface:

===========================
CLI: Command Line Interface
===========================

Talon provides a handy command line interface that allows to filter a
tractogram file and obtain the streamline coefficients in text format.

The main command is ``talon``, which is installed together with the package (see
:ref:`installation`) and allows to filter and voxelize a tractogram.


.. code::

    >>> talon --help
    usage: talon [-h] {filter,voxelize} ...

    Tractograms As Linear Operators in Neuroimaging - command line interface

    positional arguments:
      {filter,voxelize}
        filter           Filter a tractogram using TALON.
        voxelize         Voxelize a tractogram using TALON.

    optional arguments:
      -h, --help         show this help message and exit

    Copyright: CoBCoM 2021.


``talon filter``
=========================
The ``talon filter`` command allows to filter a given tractogram as in
:ref:`inverse-problem`, but without the need to write any Python code.

The basic syntax that you'll have to use is

.. code:: bash

    talon filter streamlines.tck data.nii.gz streamline_weights.txt

where ``streamline.tck`` is the tractogram to be filtered, ``data.nii.gz`` is
what is being fit by the filtering process (we will get to that later) and
``streamline_weights.txt`` is the text file where the streamline weights will
be saved.

Streamlines
-----------
The input tractogram must be in NiBabel-readable format,
i.e., in ``tck`` or ``trk`` format. In both cases, it is required to be in
RAS+ and mm space. The streamline coordinate (0,0,0) refers to the center of
the voxel.

Data
----
The input data must be a ``.nii/.nii.gz`` volume registered with the
tractogram. It contains the data fitted by talon. For the *volume-fraction*
model used by ``talon filter`` it has to encode the intra-axonal volume
fraction in each voxel.

Output weights
--------------
The output is a text file where the n-th row contains the
weight computed for the n-th streamline.

Group sparsity regularization
-----------------------------
The command is able to take into account the bundle organization of the
streamlines. For a detailed presentation of how this is encoded as a
regularization term, please refer to :ref:`structured-sparsity`.
This prior is activated by passing the option
``--streamline-assignment sa.txt`` to ``talon solve``.
The ``sa.txt`` file contains one row per streamline and the n-th row contains
the labels of the two regions connected by the n-th streamline.
For instance, a tractogram with three streamlines could correspond to the
following assignment file.

.. code:: text

    # assignment file of subject ABC1234
    3 15
    7 2
    15 3

The first row starts with ``#``, hence will not be read by the program.
Then we have a streamline connecting regions 3 and 15, a second one connecting
regions 7 and 2 and a third streamline connecting regions 15 and 3.
The order of the labels is ignored by the program, hence the first and the
third streamlines are bundled together, while the second streamline forms
another bundle.

The assignment file is typically obtained via ``tck2connectome``, which is
part of the
`Mrtrix's suite
<https://mrtrix.readthedocs.io/en/3.0.2/reference/commands/tck2connectome.html>`_.

.. code:: bash

    tck2connectome \
        streamlines.tck atlas.nii.gz connectome.txt \
        -out_assignment streamline_assignment.txt


Using GPUs
----------
Using a GPU can significantly speed up the execution. Before attempting to use
it, be sure to have `PyOpenCL <https://pypi.org/project/pyopencl/>`_ installed.
The use of the GPU processing capabilities is triggered by the
``--operator-type`` option as follows.

.. code:: bash

    --operator-type opencl


Other options
-------------

.. code::

    >>> talon filter --help
    usage: talon filter [-h] [--operator-type {reference,fast,opencl}]
                        [--ndir number] [--allow-negative-x] [--sigma value]
                        [--streamline-assignment file] [--connectome file]
                        [--objective-relative-tolerance value]
                        [--x-absolute-tolerance value] [--maxiter count]
                        [--precomputed-indices-weights file_idx file_wei]
                        [--save-generators-indices-weights file_gen file_idx file_wei | --save-operator-pickle file]
                        [--force] [--quiet | --warn | --info | --debug]
                        in_tracks in_data out_weights

    Use TALON to filter a tractogram with the Volume Fraction forward model.

    positional arguments:
      in_tracks             Input tractogram file in RAS+ and mm space. The
                            streamline coordinate (0,0,0) refers to the center of
                            the voxel. Must be in NiBabel-readable format (.trk or
                            .tck).
      in_data               Input data to be fitted. Serves also as reference
                            space for tractogram. Must be in NiBabel-readable
                            format (.nii or .nii.gz).
      out_weights           Output text file containing the streamline weights.

    optional arguments:
      -h, --help            show this help message and exit
      --operator-type {reference,fast,opencl}
                            Type of operator to use. Default: `fast`.
      --ndir number         Number of directions for the voxelization. Default:
                            1000.
      --precomputed-indices-weights file_idx file_wei
                            Uses the indices and weights passed as input to build
                            the linear operator. E.g. `--precomputed-indices-
                            weights <indices>.npz <weights>.npz`. The two matrices
                            must be defined on the same number of directions as
                            the ones that are used at the call of this script.
      --save-generators-indices-weights file_gen file_idx file_wei
                            Saves the linear operator as three separate files
                            `<generators>.npy <indices>.npz <weights>.npz`. All
                            types of operator can be saved in this format.
      --save-operator-pickle file
                            Saves the linear operator with pickle. Only available
                            when --operator-type is set to `reference` or `fast`.
      --force               Overwrite existing files.
      --quiet               Do not display messages.
      --warn                Display warning messages.
      --info                Display information messages.
      --debug               Display debug messages.

    Solver options:
      --allow-negative-x    Disables the non negativity constraint.
      --sigma value         Sets the regularization scale parameter as in (Frigo,
                            2021). The final value of lambda is
                            `sigma*max(||At*data||/gwei)`, where sigma is the
                            passed parameter, `||At*data||` is the 2-norm of the
                            product between the transposed linear operator and the
                            data, and `gwei` is the vector of the weights
                            associated to each group of streamlines. Default: 0.0.
      --streamline-assignment file
                            Activates the group sparsity regularization by
                            specifying the node assignments of each streamline to
                            some parcellation. Typically, this file is produced by
                            the Mrtrix3 command `tck2connectome` with the option
                            `-out_assignment`. The file is expected to be in text
                            format with one row per streamline. E.g., if the first
                            row is [5, 14], the first streamline will be bundled
                            together with all the streamlines corresponding rows
                            having [5, 14] or [14, 5].
      --connectome file     Activates the FIT regularization by specifying the
                            connectivity matrix. Each streamline bundle is
                            associated to the entry in the connectivity matrix
                            corresponding to the region lables that it connects.
                            E.g., the bundle connecting regions 5 and 14 is
                            associated to the entry [5, 14] of the connectivity
                            matrix. Notice that the first row and column
                            correspond to the zero label. Must be used together
                            with `--streamline-assignment`.
      --objective-relative-tolerance value
                            Sets relative tolerance on cost function. Default:
                            1e-06.
      --x-absolute-tolerance value
                            Sets absolute tolerance on variable. Default: 1e-06.
      --maxiter count       Sets maximum number of iterations. Default: 1000.



``talon voxelize``
=========================
The ``talon filter`` command allows to create the *indices* and *weights*
matrices that are necessary to define a talon linear operator as in
:ref:`getting-started`, but without the need to write any Python code.

The basic syntax that you'll have to use is

.. code:: bash

    talon voxelize streamlines.tck image.nii.gz indices.npz weights.npz

where ``streamline.tck`` is the tractogram to be voxelized, ``image.nii.gz`` is
a reference image that defines the shape of the linear operator (typically the
data that is going to be fitted in the filtering process) and ``indices.npz``
and ``weights.npz`` are the two
`COO sparse matrices <https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.coo_matrix.html>`_
that define the indices and weights of the linear operator respectively.

Streamlines
-----------
The input tractogram must be in NiBabel-readable format,
i.e., in ``tck`` or ``trk`` format. In both cases, it is required to be in
RAS+ and mm space. The streamline coordinate (0,0,0) refers to the center of
the voxel.

Output matrices
---------------
The two COO matrices are saved in ``.npz`` format. If the
suffix is not present in the filename, it is automatically appended.

Other options
-------------

.. code::

    >>> talon voxelize --help
    usage: talon voxelize [-h] [--ndir number] [--force]
                          [--quiet | --warn | --info | --debug]
                          in_tracks in_img out_ind out_wei

    Transform a tractogram into the `indices` and `weights` matrices that are used
    in the definition of the linear operator used by TALON.

    positional arguments:
      in_tracks      Tractogram file to be voxelized in RAS+ and mm space. The
                     streamline coordinate (0,0,0) refers to the center of the
                     voxel. Must be in NiBabel-readable format (.trk or .tck).
      in_img         Image serving as space reference. Must be in NiBabel-readable
                     format (.nii or .nii.gz).
      out_ind        Path where the indices will be saved in .npz format.
      out_wei        Path where the weights will be saved in .npz format.

    optional arguments:
      -h, --help     show this help message and exit
      --ndir number  Number of directions for the voxelization. Default: 1000.
      --force        Overwrite existing files.
      --quiet        Do not display messages.
      --warn         Display warning messages.
      --info         Display information messages.
      --debug        Display debug messages.

