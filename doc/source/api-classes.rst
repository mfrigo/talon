==============
Classes
==============

Linear Operator
===============
.. autoclass:: talon.core.AbstractLinearOperator
    :members:

CPU
---
.. autoclass:: talon.core.LinearOperator
    :members:

.. autoclass:: talon.fast.LinearOperator
    :members:

.. autoclass:: talon.core.ConcatenatedLinearOperator
    :members:

GPU
---
.. autoclass:: talon.opencl.LinearOperator
    :members:

Regularization Term
===================
.. autoclass:: talon.optimization.RegularizationTerm
    :members:

.. autoclass:: talon.optimization.NoRegularization
    :members:

.. autoclass:: talon.optimization.NonNegativity
    :members:

.. autoclass:: talon.optimization.StructuredSparsity
    :members:

.. autoclass:: talon.optimization.NonNegativeStructuredSparsity
    :members:

.. autoclass:: talon.optimization.ExitStatus
    :members:
