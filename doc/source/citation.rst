.. _citation:

====================
How to cite talon
====================
If you use talon in your research, please cite the package in the following
format.

    First Author, Second Author, Third Author. TALON: Tractograms As Linear
    Operators in Neuroimaging. CoBCoM, 2021.

.. code:: bibtex

    @misc{cobcomtalon,
          author = {Author, First and Author, Second and Author, Third},
          title = {TALON: Tractograms As Linear Operators in Neuroimaging},
          howpublished = {CoBCoM},
          year = {2021}
         }
