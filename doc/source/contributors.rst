.. _contributors:

====================
List of Contributors
====================
Talon was conceived in the ATHENA Project Team at Inria Sophia Antipolis -
Méditerranée. The package was initially developed within the Computational
Brain Connectivity Mapping (CoBCoM) project by:

* Samuel Deslauriers-Gauthier, ATHENA Project Team, Inria Sophia Antipolis - Méditerranée.
* Matteo Frigo , ATHENA Project Team, Inria Sophia Antipolis - Méditerranée.
* Mauro Zucchelli , ATHENA Project Team, Inria Sophia Antipolis - Méditerranée.

The project is currently maintained by:

* Samuel Deslauriers-Gauthier, ATHENA Project Team, Inria Sophia Antipolis - Méditerranée.
* Matteo Frigo , ATHENA Project Team, Inria Sophia Antipolis - Méditerranée.
