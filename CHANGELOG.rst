=========
Changelog
=========

All notable changes to the `talon` project are documented in this file.

talon 0.3
========================

Documentation
-------------

- Added a changelog file to record all notable changes to the `talon` project.

- Improved the performance of the dot product for the transposed OpenCL linear
  operator.

- Added command line interface to filter tractograms.

- Updated documentation with new examples and preparation for readthedocs.org.

- Added a convenience function for the zero operator.

- Implemented a data mask for sparse imaging data.

- Added coverage information to CI testing.

- Various performance improvements.
